<?php  include("check.php"); ?>  
<!DOCTYPE html>
<html lang="en">
<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>REVL</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="style.css">

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
	<script src="jquery.table2excel.js"></script> 




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
 
 <style>
 .nomore { background-color:#e9c2c2; font-size:14px; border-radius:4px; padding:5px;}
 .ok { background-color:#bef1c8; font-size:14px; border-radius:4px; padding:5px;}
 .even { background-color:#fff; color:#333; font-weight:normal;}
 .odd { background-color:#f0f0f0; color:#333; font-weight:normal;}
 </style>
  </head>
  <body>
  <?php 

  ?>
   <?php  include("header.php");
   require ("Class-Search.php"); 
   $search = new Search($DB);

   ?>
<style>
#downLoad {
	float:right;
	padding:10px;
	background-color:#272727;
	color:#fff;
	cursor:pointer;

}
</style>
    <div class="pageHeader">
      <div class="container">
        <b><div id="downLoad">Export</div></b>
        <h2>View Codes</h2>
      </div><!--End container-->
    </div><!--end pageHeader-->


<?php 
$searchcode=addslashes($_POST['code']);
$searchtype=$_POST['codetype'];
$WHERE = Array();
 
if ($searchcode) {
$WHERE[] = "(code like '%".$searchcode."%')";		
	}
if ($searchtype) {
$WHERE[] = "(codetype =".$searchtype.")";	
	}
	
	if (($searchcode)or($searchtype)) {
$sql = "SELECT *
				FROM codes WHERE ".implode(" AND ", $WHERE)."
		ORDER BY dateAdded desc"; 
		
		}else{
			
$sql = "SELECT *
				FROM codes 
		ORDER BY dateAdded desc"; 
			}
	
//$getCodes = $DB->prepare($sql);
//$getCodes->execute();

$search->setQuery($sql);
$search->setResPerPage(50);
$search->setPage($_GET['page']);
$search->execute();	


?>
  <section class="content">
      <div class="container">
        <div class="resultsContainer">
        
    <?php 
   //PAGE LINKS
if ($search->getTotalPages()>1){
	$link = "view.php?"; ?>
	<div class="pagelinks">
		<?php 
		$pages = $search->getPages();
		foreach ($pages as $page){
			if ($page['current']==true) {?>
				<span class="current"><?php echo $page['text']?></span>
			<?php } else {?>
				<a href="<?php echo $link.'page='.$page['page'];?>"><?php echo $page['text']?></a>
			<?php }?>
		<?php }?>
	Results: <?php echo $search->getNumRows()?></div>
<?php  } ?> 

        
        
          <table class="table table2excel" id="viewtable" data-tableName="REVL Codes">
	 <thead>
             <form action="#" method="post">
              <tr>
                <th>Date of Creation</th>
                <th>Code<br>
                <input type="text" name="code" class="form-control"  <?php  if ($searchcode) { echo "value='".$searchcode."'";}?>></th>
                <th>Type<br>  <select name="codetype" class="form-control" style="width:75%; float:left;">
             
               
     
              <option value="">Select</option>
              <option value="1" <?php  if ($searchtype==1) { echo "selected";}?>>for REVL</option>
              <option value="2" <?php  if ($searchtype==2) { echo "selected";}?>>for users (via Referral)</option>
              <option value="3" <?php  if ($searchtype==3) { echo "selected";}?>>for brands</option>
              <option value="4" <?php  if ($searchtype==4) { echo "selected";}?>>for users (via App)</option>
            </select>
            <input type="submit" value="GO" class="form-control" style="width:22%;">
            </th>
                <th>Use</th>
                <th>Volume</th>
                <th>Time limit</th>
                <th>Notes</th>
                <th>Remaining</th>
              </tr></form>
            </thead>
	<tbody>
		<?php  
		
//if ($search->getNumRows()>0){
$codes = $search->getResults(); 
foreach ($codes as $c){		
//	while ($c = $getCodes->fetch(PDO::FETCH_ASSOC)){		
	?>
			<tr 
			    <?php 
if (($c['codetype']=="2")or($c['codetype']=="4")) {
if ($c['group'] % 2 == 0) {
  echo "class='even noExl'";
} else{
echo "class='odd noExl'";
}	
	
}else{

if ($c['group'] % 2 == 0) {
  echo "class='even'";
} else{
echo "class='odd'";
}	
	}
				

				?>
			>
            
             
				<td><?php echo $c['dateAdded']?></td>
				<td style="text-transform:uppercase"><?php echo $c['code']?></td>
				<td>
				
				
    
           
                  <?php 
                  if ($c['codetype']=="1") {
					 echo "for REVL"; 
					  }
	                  if ($c['codetype']=="2") {
					 echo "for users (via Referral)"; 
					 if ($c['referralCode']) {
					 echo "<br><span class='label label-warning'>Referral Code: ".$c['referralCode']."</span>";
					 
					 }
					  }
	                  if ($c['codetype']=="3") {
					 echo "for brands"; 
					  }
	                  if ($c['codetype']=="4") {
					 echo "for users (via App)"; 
					  }
				  ?>
				
                
				</td>
				<td>
				<?php 
                if ($c['codeuse']==1) {
					echo "Single";
					}else{
					echo "Multiple";	
						}
				?>
                </td>
				<td>
				  
				
				<?php echo $c['multiple']?></td>
				<td>
				<?php 
                if ($c['expires']==1) {
					

date_default_timezone_set('Europe/Athens');
$nowdate=date("Y-m-d H:i:s");
$datetime = new DateTime($nowdate);
$dateathens=$datetime->format('Y-m-d H:i:s');
$la_time = new DateTimeZone('Europe/London');
$datetime->setTimezone($la_time);
$dateuknow=$datetime->format('Y-m-d H:i:s');

if ($dateuknow<$c['dateExpires']) {
	?>
    <?php 
    if ($c['active']==0) {
	?>
<span class='label label-warning'>1-DAY after assignment</span>	
	<?php 	
	}else{
	?>
<span class='label label-success'>Expires: <?php  echo $c['dateExpires']; ?></span>	
	<?php 
	}
	}else{
	?>
	<span class='label label-danger'>Expired</span>
	<?php 	
		}					
					

					}else{
					echo "No Limit";	
						}
				?>
                </td>
				<td><?php echo $c['notes']?></td>
				<td><div <?php  if ($c['remaining']==0) {echo 'class=nomore'; }else{ echo 'class=ok';} ?>>
				<?php echo $c['remaining']?></div>
                <?php 
                if (($c['codetype']==2)or($c['codetype']==4)) {
					if ($c['active']==1) {
						echo "<span class='label label-info'>assigned</span>";
						}else{
						echo "<span class='label label-danger'>not assigned</span>";
						}
					}
				?>
                
                </td>
				
			
			</tr>
		<?php }?>
	</tbody>
</table></div></div></section>


    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript">

    $( document ).ready(function() {
		$('#codeuse').change(function(){		
			if ($(this).val()=='multiple') $('.multiplenum').addClass('active');
			else $('.multiplenum').removeClass('active');
		});
    });     

    </script>
    
    <script>
			$('#downLoad').click(function() {
				$(".table2excel").table2excel({
					exclude: ".noExl",
					name: "REVL Codes",
					filename: "REVL-Codes",
					fileext: ".xls",
					exclude_img: true,
					exclude_links: true,
					exclude_inputs: true
				});
			});
		</script>
    
  </body>
</html>