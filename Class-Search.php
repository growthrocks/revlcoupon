<?php 
//namespace Search;
use \PDO;
 
class Search {	
	protected $query, $resPerPage, $numRows, $totalPages, $page, $offset, $pages = Array(), $results = Array(), $args=Array(), $DB;
	
	function __construct($DB){							
		$this->DB = $DB;
		$this->setResPerPage(40);
		$this->setLinkRange(3);	
		$this->setPage(1);		
	}	
		
	function execute(){
		$this->createPages();
		$this->runQuery();
	}
	
	function createPages(){		
		$st = $this->DB->prepare($this->getQuery());		
		$st->execute($this->args);		
		
		$this->setNumRows($st->rowCount());
		
			
		$this->setTotalPages( ceil( $this->getNumRows()/$this->getResPerPage() ) );
			
		if ($this->getPage() > $this->getTotalPages() ) $this->setPage( $this->getTotalPages() ); // SET CURRENT PAGE TO LAST PAGE
		if ($this->getPage()<1) $this->setPage(1);
		
		$this->setOffset( ($this->getPage() - 1) * $this->getResPerPage() );
		
		$this->pages = Array();
		if ($this->getPage() > 1) //NOT ON FIRST PAGE
		{
			$temp = Array();
			$temp["text"] = "<<";
			$temp["page"] = 1;	
			$temp["current"] = false;				
			$this->pages[] = $temp;
			
			$temp = Array();
			$temp["text"] = "<";
			$temp["page"] = $this->getPage() - 1;			
			$temp["current"] = false;				
			$this->pages[] = $temp;
		}
		for ($x = $this->getPage()-$this->getLinkRange(); $x < $this->getPage()+$this->getLinkRange()+1; $x++) {
			if ($x > 0 AND $x <= $this->getTotalPages()) { //IF IT'S A VALID PAGE NUMBER
				$temp = Array();
				$temp["text"] = $temp["page"] = $x;
				if ($x == $this->getPage()) $temp["current"] = true;		
				else $temp["current"] = false;		
				
				$this->pages[] = $temp;
			}
		}
		
		if ($this->getPage() != $this->getTotalPages()) { //NOT ON LAST PAGE
			$temp = Array();
			$temp["text"] = ">";
			$temp["page"] = $this->getPage() + 1;			
			$temp["current"] = false;				
			$this->pages[] = $temp;
			
			$temp = Array();
			$temp["text"] = ">>";
			$temp["page"] = $this->getTotalPages() + 1;			
			$temp["current"] = false;				
			$this->pages[] = $temp;			
		}
	}
	function getPages() {
		return $this->pages;
	}	
	
	function setQuery($q,$args=Array()){ 
		$this->query = $q; 
		if (sizeof($args)>0) $this->args = $args;
	}
	function getQuery(){ return $this->query; }
	function setResPerPage($num){ $this->resPerPage = $num; }
	function getResPerPage(){ return $this->resPerPage; }
	
	function setLinkRange($num){ $this->linkRange = $num; }
	function getLinkRange(){ return $this->linkRange; }
	function setNumRows($num){ $this->numRows = $num; }
	function getNumRows(){ return $this->numRows; }
	
	function setTotalPages($num){ $this->totalPages = $num; }
	function getTotalPages(){ return $this->totalPages; }
	
	function setPage($num){ $this->page = $num; } //the current page for pagination
	function getPage(){ return $this->page; } 
	
	function setOffset($num){ $this->offset = $num; }
	function getOffset(){ return $this->offset; }
	
	
	function getMainSql(){		
		$sql = $this->getQuery()." LIMIT ".$this->getOffset().", ".$this->getResPerPage();
		return $sql;		
	}
	
	function runQuery(){
		$this->results = Array();
		$st = $this->DB->prepare($this->getMainSql());
		$st->execute($this->args);
		$this->results = $st->fetchAll(PDO::FETCH_ASSOC);		
	}
	
	function getResults(){
		return $this->results;
	}
}
?>