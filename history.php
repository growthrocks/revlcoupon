<?php  include("check.php"); ?>  
<!DOCTYPE html>
<html lang="en">
<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>REVL</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="style.css">

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
 
 
 <style>
 .nomore { background-color:#e9c2c2; font-size:14px;}
 .ok {background-color:#bef1c8;}
 </style>
  </head>
  <body>
   <?php  include("header.php"); 
   require ("Class-Search.php"); 
   $search = new Search($DB);
   ?>  
   

    <div class="pageHeader">
      <div class="container">
        <h2>View Codes</h2>
      </div><!--End container-->
    </div><!--end pageHeader-->




 <?php 
 /*
 
<?php 
for ($x = 0; $x <= 10; $x++) {
$thedate=date('Y-m-d', strtotime('today - '.$x.' days'));
$st = $DB->prepare("SELECT count(*) as thecount,api FROM codesHistory where requestDate like '".$thedate."%' group by api");
//echo "SELECT count(*) as thecount,api FROM codesHistory where requestDate like '".$thedate."%' group by api<br>";
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
       $theresultrow.= $row['thecount'].",";    
}
if ($theresultrow=='') {
$thetotalresult.= '[\''.$thedate.'\',0,0],';
}else{
$thetotalresult.= '[\''.$thedate.'\','.substr($theresultrow, 0,-1)."],";
	
}
$theresultrow='';
} 
?>



<?php 
for ($x = 0; $x <= 10; $x++) {
$thedategraph2=date('Y-m-d', strtotime('today - '.$x.' days'));

$st = $DB->prepare("SELECT distinct requestReturnText FROM codesHistory where apisuccess=0 order by requestReturnText");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
    if ($x==1) {
	   $thetitles.= '\''.$row['requestReturnText']."',";    
	}
$st2 = $DB->prepare("SELECT count(*) as thecount2 FROM codesHistory where apisuccess=0 and requestDate like '".$thedategraph2."%' and requestReturnText='".$row['requestReturnText']."'");
        $st2->execute(); 
        while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)){
$thecounts.=$row2['thecount2'].",";
	   //['2016-09-18',3,4,8,25]

}
$therowcounts .= $thecounts;
$thecounts='';
}
$finaldata.= '[\''.$thedategraph2.'\','.substr($therowcounts,0,-1)."],";
$therowcounts='';
}

//echo $thetitles;
//echo ''.$finaldata.'';

?>




<div class="container">
<div class="row">


<div class="col-md-6">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

 

      var data = google.visualization.arrayToDataTable([
        ['Date', 'Codes Given', 'Codes Asked'],
       <?php 
	   echo substr($thetotalresult,0,-1);
	   ?>
      ]);
	  
	    var data2 = google.visualization.arrayToDataTable([
      <?php 
	  echo '[Date,'.substr($thetitles,0,-1).'],';
echo ''.substr($finaldata,0,-1).'';

	  ?>
      ]);

      var options = {
        width: 600,
        height: 400,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '95%' },
		 colors: ['#f8f676', '#333', '#000', '#ccc', '#eee'],
        isStacked: true,
      };

        // Instantiate and draw our chart, passing in some options.
 var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(data, options);
	 var chart2 = new google.visualization.ColumnChart(document.getElementById("columnchart_values2"));
      chart2.draw(data2, options);
	        }
    </script>
    <div id="columnchart_values"></div>

</div>
<div class="col-md-6">
    <div id="columnchart_values2"></div>

</div>
</div>
</div>
*/
 ?><?php 
$searchcode=addslashes($_POST['code']);
$responsetext=$_POST['responsetext'];
$WHERE = Array();
 
if ($searchcode) {
$WHERE[] = "(code like '%".$searchcode."%')";		
	}
if ($responsetext) {
$WHERE[] = "(requestReturnText like '%".$responsetext."%')";	
	}
	
	if (($searchcode)or($responsetext)) {
$sql = "SELECT *
				FROM codesHistory WHERE ".implode(" AND ", $WHERE)."
		ORDER BY requestDate desc"; 
		
		}else{
			
$sql = "SELECT *
				FROM codesHistory 
		ORDER BY requestDate desc"; 
			}
	


	if (($searchcode)or($searchtype)) {
$sql = "SELECT *
				FROM codesHistory WHERE ".implode(" AND ", $WHERE)."
		ORDER BY requestDate desc"; 
		}else{
			
$sql = "SELECT *
				FROM codesHistory
		ORDER BY requestDate desc"; 
			}
	

//$getCodes = $DB->prepare($sql);
//$getCodes->execute();

$search->setQuery($sql);
$search->setResPerPage(20);
$search->setPage($_GET['page']);
$search->execute();	


?>

  <section class="content">
      <div class="container">
        <div class="resultsContainer">
         <?php 
   //PAGE LINKS
if ($search->getTotalPages()>1){
	$link = "history.php?"; ?>
	<div class="pagelinks">
		<?php 
		$pages = $search->getPages();
		foreach ($pages as $page){
			if ($page['current']==true) {?>
				<span class="current"><?php echo $page['text']?></span>
			<?php } else {?>
				<a href="<?php echo $link.'page='.$page['page'];?>"><?php echo $page['text']?></a>
			<?php }?>
		<?php }?>
	Results: <?php echo $search->getNumRows()?></div>
<?php  } ?> 

        
          <table class="table">
	 <thead>
              <form action="#" method="post">   <tr>
                <th>Request Date</th>
                <th>Response
              <input type="text" name="responsetext" class="form-control"  <?php  if ($responsetext) { echo "value='".$responsetext."'";}?>>
                </th>
                <th>Code<br><input type="text" name="code" class="form-control" style="float:left; width:75%;"  <?php  if ($searchcode) { echo "value='".$searchcode."'";}?>>
                <input type="submit" value="GO" class="form-control" style="width:22%;">
                </th>
                <th>Request Received</th>
                <th>Api</th>
                <th>IP</th>
              </tr></form>
            </thead>
	<tbody>
			<?php  
		
//if ($search->getNumRows()>0){
$history = $search->getResults(); 
foreach ($history as $c){		
//	while ($c = $getCodes->fetch(PDO::FETCH_ASSOC)){		
	?>
			<tr <?php  if (($c['requestReturnText']=='Valid')or($c['apisuccess']==1)) {echo 'class=ok'; }else{ echo 'class=nomore';} ?>>
				<td><?php echo $c['requestDate']?></td>
				<td>
				<?php 
                echo $c['requestReturnText'];
				?>
                </td>
                <td style="text-transform:uppercase"><?php echo $c['code']?></td>
				<td style="max-width:300px;">
                <div style="word-wrap: break-word;">
				<small>
				<?php 
                echo str_replace("http://9amlabs.com/work/golive/growthrocks/revl/","","".$c['requestReceived']."");

				?>
				<?php //=$c['requestReceived']?></small>
				
           </div>
				</td>
				
				<td><?php  
				if ($c['api']==1) {
					echo 'Code Receiver';
					}
					if ($c['api']==2) {
					echo 'Code Giver';
					}
				
				?></td>
				<td><?php  echo $c['ip'];?></td>
				
				
			
			</tr>
		<?php }?>
	</tbody>
</table></section></div></div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  </body>
</html>