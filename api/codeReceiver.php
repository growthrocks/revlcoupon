<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

include("../config.php");
$codeSent=$_GET['codeS'];
$apiKey=$_GET['apiK'];
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$userIp=$_SERVER['REMOTE_ADDR'];

//check that code was sent and api K is correct
if (($codeSent!='') and ($apiKey)=="Dallas")
	{
	
	
//check if code exists in TYPE 1&3 (Reserved for REVL & Brands)
//$st = $DB->prepare("SELECT * FROM codes WHERE code=:codeS AND ((`codetype` =1)OR (`codetype` =3)) LIMIT 1");
$st = $DB->prepare("SELECT * FROM codes WHERE code=:codeS and active=1 LIMIT 1");
$args = array(
	":codeS" => $codeSent
	);
$st->execute($args);
$codeValid = $st->fetch(PDO::FETCH_ASSOC);

if ($codeValid['id']>0) {
	
	
date_default_timezone_set('Europe/Athens');
$nowdate=date("Y-m-d H:i:s");
$datetime = new DateTime($nowdate);
$dateathens=$datetime->format('Y-m-d H:i:s');
$la_time = new DateTimeZone('Europe/London');
$datetime->setTimezone($la_time);
$dateNowUk=$datetime->format('Y-m-d H:i:s');
//	echo $dateNowUk;
	
	//VALID CODE. Now check if the code has expired
	$checkExpiration = $DB->prepare("SELECT * FROM codes WHERE code=:codeS and dateExpires>:dateNowUk LIMIT 1");
$argsExpiration = array(
	":codeS" => $codeValid['code'],
	":dateNowUk" => $dateNowUk
	);
$checkExpiration->execute($argsExpiration);
$isitExpired = $checkExpiration->fetch(PDO::FETCH_ASSOC);

if ($isitExpired['id']>0) {
	
	
	
	
	//VALID CODE and NOT EXPIRED. Now check if the code has remaining for redemption
$stOK = $DB->prepare("SELECT * FROM codes WHERE code=:codeS and remaining>0 LIMIT 1");
$argsOK = array(
	":codeS" => $codeValid['code']
	);
$stOK->execute($argsOK);
$codeOK = $stOK->fetch(PDO::FETCH_ASSOC);

if ($codeOK['id']) {
	$response="Valid"; 
	
	//UPDATE REMAINING 
	//TO DO > QUERY NEEDS TO BE CHANGED
$updateRemaining= "update codes set remaining=remaining-1 where code='".$codeOK['code']."'";
$update = $DB->prepare($updateRemaining);
$update->execute();



/*

*/
	
	
	}else{
	$response="Invalid - Redeemed";
}

}else{
	$response="Invalid - Expired";
	}
		
		
	
	
	
	
	}else{
	
	$response="Invalid - Code or Type";
	
	
	}
	
	
	
	
	
	
	
	}else{
		//LOG THIS TO DB
	$response="Invalid API Request";
}

echo $response;
if (!$codeSent) {
	$codeSent='';
	}
$insertHistory= "INSERT INTO `codesHistory`(`code`, `requestDate`, `requestReceived`, `requestReturn`, `requestReturnText`,`ip`,`api`) VALUES (:codeSent,now(),:actualLink,'',:response,:userIp,1)";

$args = Array();
$args[':codeSent'] = $codeSent;
$args[':actualLink'] = $actual_link;
$args[':response'] = $response; 
$args[':userIp'] = $userIp;
$st = $DB->prepare($insertHistory);
$st->execute($args);

?>
