<?php  include("check.php"); ?>
<!DOCTYPE html>
<html lang="en">
<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>REVL</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
.nomore {
	background-color: #e9c2c2;
	font-size: 14px;
}
.ok {
	background-color: #bef1c8;
}
</style>
</head>
<body>
<?php  include("header.php"); 
   require ("Class-Search.php"); 
   $search = new Search($DB);
   ?>
<div class="pageHeader">
  <div class="container">
    <h2>Dashboard</h2>
  </div>
  <!--End container--> 
</div>
<!--end pageHeader-->

<section class="content">
  <div class="container">
    <div class="resultsContainer">
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>for REVL</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
$st = $DB->prepare("SELECT count(*) as thecount FROM codes where codetype=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                  <tr>
                    <td style="font-size:45px;"><?php  echo $row['thecount'];?> <span class="label label-success" style="font-size:12px; display:block;">assigned</span></td>
                  </tr>
                  <?php 
    
    }

  

 ?>
                </tbody>
              </table>
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Sum of Remaining</th>
                    <th>Count of Remaining</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php 
$st = $DB->prepare("SELECT sum(remaining) as thesum FROM codes where codetype=1 and remaining>=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                    <td style="font-size:25px;"><?php  echo $row['thesum'];?></td>
                    <?php  } ?>
                    <?php 
$st = $DB->prepare("SELECT count(distinct code) as thecount FROM codes where remaining>=1 and codetype=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                    <td style="font-size:25px;"><?php  echo $row['thecount'];?></td>
                    <?php  } ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th colspan="2">for user (via Referral)</th>
                  </tr>
                </thead>
                <tbody>
         
                  <tr>
             
                      <?php 
$st = $DB->prepare("SELECT count(*) as thecount FROM codes where codetype=2 and active=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>       <td style="font-size:45px;"><?php  echo $row['thecount'];?> <span class="label label-success" style="font-size:12px; display:block;">assigned</span></td>                <?php 
    
    }

  ?>
  
     <?php 
$st = $DB->prepare("SELECT count(*) as thecount FROM codes where codetype=2 and active=0");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>       <td style="font-size:45px;"><?php  echo $row['thecount'];?> <span class="label label-warning" style="font-size:12px; display:block;">unassigned</span></td>                <?php 
    
    }

  

 ?>
              
                  </tr>
  
                </tbody>
              </table>
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Sum of Remaining (Assigned)</th>
                    <th>Count of Remaining (Assigned)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php 
$st = $DB->prepare("SELECT sum(remaining) as thesum FROM codes where codetype=2 and remaining>=1 and active=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                    <td style="font-size:25px;"><?php  echo $row['thesum'];?></td>
                    <?php  } ?>
                    <?php 
$st = $DB->prepare("SELECT count(distinct code) as thecount FROM codes where remaining>=1 and codetype=2 and active=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                    <td style="font-size:25px;"><?php  echo $row['thecount'];?></td>
                    <?php  } ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-3">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <table class="table table-hover table-striped">
                        <thead>
                          <tr>
                            <th>for brands</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
        $st = $DB->prepare("SELECT count(*) as thecount FROM codes where codetype=3");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
           
        ?>
                          <tr>
                            <td style="font-size:45px;"><?php  echo $row['thecount'];?> <span class="label label-success" style="font-size:12px; display:block;">assigned</span></td>
                          </tr>
                          <?php 
            
            }
        
          
        
         ?>
                        </tbody>
                      </table>
                      <table class="table table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Sum of Remaining</th>
                            <th>Count of Remaining</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <?php 
        $st = $DB->prepare("SELECT sum(remaining) as thesum FROM codes where codetype=3 and remaining>=1");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
           
        ?>
                            <td style="font-size:25px;"><?php  echo $row['thesum'];?></td>
                            <?php  } ?>
                            <?php 
        $st = $DB->prepare("SELECT count(distinct code) as thecount FROM codes where remaining>=1 and codetype=3");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
           
        ?>
                            <td style="font-size:25px;"><?php  echo $row['thecount'];?></td>
                            <?php  } ?>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>      
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th colspan="2">for user (via App)</th>
                  </tr>
                </thead>
                <tbody>
         
                  <tr>
             
                      <?php 
$st = $DB->prepare("SELECT count(*) as thecount FROM codes where codetype=4 and active=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>       <td style="font-size:45px;"><?php  echo $row['thecount'];?> <span class="label label-success" style="font-size:12px; display:block;">assigned</span></td>                <?php 
    
    }

  ?>
  
     <?php 
$st = $DB->prepare("SELECT count(*) as thecount FROM codes where codetype=4 and active=0");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>       <td style="font-size:45px;"><?php  echo $row['thecount'];?> <span class="label label-warning" style="font-size:12px; display:block;">unassigned</span></td>                <?php 
    
    }

  

 ?>
              
                  </tr>
  
                </tbody>
              </table>
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Sum of Remaining (Assigned)</th>
                    <th>Count of Remaining (Assigned)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php 
$st = $DB->prepare("SELECT sum(remaining) as thesum FROM codes where codetype=4 and remaining>=1 and active=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                    <td style="font-size:25px;"><?php  echo $row['thesum'];?></td>
                    <?php  } ?>
                    <?php 
$st = $DB->prepare("SELECT count(distinct code) as thecount FROM codes where remaining>=1 and codetype=4 and active=1");
$st->execute(); 
while ($row = $st->fetch(PDO::FETCH_ASSOC)){
   
?>
                    <td style="font-size:25px;"><?php  echo $row['thecount'];?></td>
                    <?php  } ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
      
      <div class="col-md-6">
      
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">API Status</h3>
  </div>
  <div class="panel-body">
 <table width="100%"> <tr>
  <?php 
        $st = $DB->prepare("SELECT count(*) as thecount,api FROM codesHistory group by api");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
           
        ?>
                            <td style="text-align:center;"><?php 
							if ($row['api']==1) {
								echo "Code Given";
								}else{
								echo "Code Asked";
									}
							 ?><br> 
                           <div style="font-size:45px;"> <?php  echo $row['thecount'];?></div></td>
                          
                                                     <?php  } ?>
                                                      </tr></table> 
  </div>
</div>
      </div>
      
      
         <div class="col-md-6">
      
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">API Responses</h3>
  </div>
  <div class="panel-body">
 <table width="100%">
  <?php 
        $st = $DB->prepare("SELECT count(*) as thecount,requestReturnText FROM codesHistory where apisuccess=0 group by requestReturnText");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
           
        ?> <tr style="border-bottom:thin solid #eee;">
                            <td style="text-align:left;">
							<?php 
							
							echo $row['requestReturnText'];
							 ?></td>
                       <td>   <?php  echo $row['thecount'];?></td>
                           </tr>
                                                     <?php  } ?>
                                                     
 <?php 
 //incorrect (api success 1 includes invalid api calls for codeGiver
        $st = $DB->prepare("SELECT count(*) as thecount FROM codesHistory where apisuccess=1");
        $st->execute(); 
        while ($row = $st->fetch(PDO::FETCH_ASSOC)){
           
        ?> <tr style="border-bottom:thin solid #eee;">
                            <td style="text-align:left;">
							Codes Served</td>
                       <td>   <?php  echo $row['thecount'];?></td>
                           </tr>
                                                     <?php  } ?>                                                     
                                                     </table> 
  </div>
</div>
      </div>
      
      </div>
    </div>
  </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>