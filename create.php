<?php  include("check.php"); ?>
<!DOCTYPE html>

<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>REVL</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="animate.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php  include("header.php"); ?>
<?php 
   
if ($_POST['num']){
	$codestobecreated=$_POST['num'];

function randText($min=10, $max=20, $randtext='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    if($min < 1) $min=1;
    $varlen = rand($min,$max);
    $randtextlen = strlen($randtext);
    $text = '';

    for($i=0; $i < $varlen; $i++) $text .= substr($randtext, rand(1, $randtextlen), 1);
    return $text;
}
function randNumbers($min=10000000, $max=20000000, $randtext='1234567890') {
    if($min < 1) $min=1;
    $varlen = rand($min,$max);
    $randtextlen = strlen($randtext);
    $text = '';

    for($i=0; $i < $varlen; $i++) $text .= substr($randtext, rand(1, $randtextlen), 1);
    return $text;
}


$groupNum = $DB->prepare("SELECT * FROM codes order by `group` desc");
$groupNum->execute();
$thegroupNum = $groupNum->fetch(PDO::FETCH_ASSOC);
if ($thegroupNum) {
$newgroupnum=$thegroupNum['group']+1;	
	}else{
$newgroupnum=1;	
		
		}
		
		
if ($_POST['multiple']>0) {
	$multiple = $_POST['multiple'];
	}else{
	$multiple=1;				
		}
	
	if ($_POST['codeuse']==1) {
		$remaining=1;
		}
		else
		{
		$remaining=$multiple;
		}	



date_default_timezone_set('Europe/Athens');
$nowdate=date("Y-m-d H:i:s");
$datetime = new DateTime($nowdate);
$dateathens=$datetime->format('Y-m-d H:i:s');
$la_time = new DateTimeZone('Europe/London');
$datetime->setTimezone($la_time);
$dateuk=$datetime->format('Y-m-d H:i:s');
$dateexpires = date('Y-m-d H:i:s', strtotime($dateuk . ' +1 day'));

if ($_POST['expires']==0) {
$dateexpires = '2100-01-01';

	}

//TO DO - NEEDS TO CHECK FOR DUPLICATES
//TO DO - NEEDS TO CHECK FOR DUPLICATES
if ($_POST['codetype']==1) {
$codeActiveInactive=1;	
	}
if ($_POST['codetype']==2) {
$codeActiveInactive=0;	
	}
if ($_POST['codetype']==3) {
$codeActiveInactive=1;
	}
if ($_POST['codetype']==4) {
$codeActiveInactive=0;	
	}


	$sql = "INSERT INTO `codes`
			(`code`,  `dateAdded`, `active`, `dateExpires`,  `notes`, `codetype`, `codeuse`, `multiple`, `expires`, `remaining`, `group`) VALUES (:code, :datetime, :active, :dateexpires,  :notes,:codetype,:codeuse,:multiple,:expires,:remaining,:group)";
	$insert = $DB->prepare($sql);
	for ($i=0;$i<$codestobecreated;$i++){
		
		//CHECK FOR DUPLICATES HERE
		
		$insert->execute(array(
		":code" => strtoupper("REVL".randNumbers(2, 2).randText(3, 3)),
		":notes" => $_POST['notes'],
		":codetype" => $_POST['codetype'],
		":active" => $codeActiveInactive,
		":codeuse" => $_POST['codeuse'],
		":multiple" => $multiple,
		":expires" => $_POST['expires'],
		":remaining" => $remaining,
		":datetime" => $dateuk,
		":dateexpires" => $dateexpires,
		":group" => $newgroupnum
		));	
		

	}
			$info[] = "".$_POST['num']." Codes have been created";
}

?>
<?php  if (sizeof($error)>0) echo  "<div class='alert alert-block alert-danger animated fadeIn' style='margin-bottom:0px; text-align:center;'>".implode("", $error)."</div>"; ?>
<?php  if (sizeof($info)>0) echo  "<div class='alert alert-success animated fadeIn' style='margin-bottom:0px; text-align:center;'>".implode("", $info)."</div>"; ?>
<div class="pageHeader">
  <div class="container">
    <h2>Add Codes</h2>
  </div>
  <!--End container--> 
</div>
<!--end pageHeader-->

<section class="content">
  <div class="container">
    <form action="create.php" class="form" method="POST">
      <div class="row">
        <div class="col-sm-2">
          <h3>Choose Quantity</h3>
          <small>To be Created</small>
          <div class="select-wrapper">
            <select name="num">
              <option value="1">1</option>
              <option value="5">5</option>
              <option value="10">10</option>
              <option value="15">15</option>
              <option value="50">50</option>
              <option value="100">100</option>
              <option value="200">200</option>
              <option value="500">500</option>
            </select>
          </div>
          <!--End select-wrapper--> 
        </div>
        <div class="col-sm-3">
          <h3>Code Type</h3>
          <small>Reserved, Users, Brands</small>
          <div class="select-wrapper">
            <select name="codetype">
            
               
     
              <option value="1">for REVL</option>
              <option value="2">for users (via Referral)</option>
              <option value="3">for brands</option>
              <option value="4">for users (via App)</option>
            </select>
          </div>
          <!--End select-wrapper--> 
        </div>
        <div class="col-sm-2">
          <h3>Code Use</h3>
          <small>Unique or Multiple</small>
          <div class="select-wrapper">
            <select name="codeuse" id="codeuse">
              <option value="1">Unique</option>
              <option value="2">Multiple</option>
            </select>
          </div>
          <!--End select-wrapper--> 
          
        </div>
        <div class="col-sm-2">
          <h3>Time Limit</h3>
          <small>Expires after 1 Day?</small>
          <div class="select-wrapper">
            <select name="expires">
              <option value="0">No</option>
              <option value="1">Yes</option>
            </select>
          </div>
          <!--End select-wrapper--> 
        </div>
        <div class="col-sm-3">
          <h3>Notes</h3>
          <small>i.e Brand Name</small>
          <div class="">
            <input title="notes" name="notes" type="text">
          </div>
          <!--End select-wrapper--> 
        </div>
        <div class="col-sm-12 multiplenum"> <small><strong>Multiple Codes: </strong>Number of times each code can be used (i.e. 5, 100, 500</small>
          <input type="text" name="multiple" value="">
        </div>
        <div class="col-sm-12">
          <button type="submit">Create Codes <i class="fa fa-plus"></i></button>
        </div>
      </div>
      <!--End row-->
    </form>
  </div>
  <!--End container--> 
</section>
<!--End content--> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
<script type="text/javascript">

    $( document ).ready(function() {
		$('#codeuse').change(function(){		
			if ($(this).val()=='2') $('.multiplenum').addClass('active');
			else $('.multiplenum').removeClass('active');
		});
    });     

    </script>
</body>
</html>